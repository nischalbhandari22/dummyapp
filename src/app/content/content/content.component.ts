import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, EmailValidator, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/reusable/confirmation-dialog/confirmation-dialog.component';

export interface employeeInterface {
  employeeName: string;
  phoneNumber: number;
  dateOfBirth: string;
  gender: string;
  department:string;
  email:EmailValidator;
  address:string;
  
}
export interface department {
  value: string;
  
}

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})

export class ContentComponent implements OnInit {
  @ViewChild('table') table : MatTable<Element>;
  hide=true;
  contentForm : FormGroup;
  displayedColumns: string[] = ['employeeName', 'phoneNumber', 'dateOfBirth', 'gender' , 'department','email', 'address','action'];
  EmployeeList: employeeInterface [] = [];
  departments: department[] = [
    {value: ' Computer'},
    {value: 'Management'},
    {value: 'Cordinator'}
  ];
  confirmationDialog:MatDialogRef<ConfirmationDialogComponent>
  update = false;
  updateIndex = -1;
  serverName :String = "Carolina";
  version : any = '2021.bet.v2';
  btnDisabled : boolean = true;
  allowText:any;
  opened = false;
  url="https://c4.wallpaperflare.com/wallpaper/557/172/58/minimalism-background-mask-anonymous-wallpaper-preview.jpg";
  selectFile(event:any){
    if(event.target.files){
      var reader = new FileReader()
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (event: any)=>{
        this.url = event.target.result
      }
    }
  }


  text:string = 'Management is the faculty'
  imageURL:String = '/assets/0.jpeg'
  isSubmit= false;
  constructor(private router:Router,formBuilder:FormBuilder , private dialog:MatDialog) {
    this.contentForm = formBuilder.group({
      employeeName : ['' , [Validators.required]],
      phoneNumber : ['' , [Validators.required]],
      dateOfBirth:['' , [Validators.required]],
      gender: ['1' , [Validators.required]],
      department:['',[Validators.required]],
      email:['',[Validators.required]],
      address:['',[Validators.required]]
      
    });
   }

  ngOnInit(): void {
    

  }


  onSubmit = () =>{
    this.isSubmit = true;
    if(this.contentForm.valid){
      if(this.update){
      this.EmployeeList[this.updateIndex].dateOfBirth = this.contentForm.controls.dateOfBirth.value;
      this.EmployeeList[this.updateIndex].employeeName = this.contentForm.controls.employeeName.value;
      this.EmployeeList[this.updateIndex].phoneNumber = this.contentForm.controls.phoneNumber.value;
      this.EmployeeList[this.updateIndex].gender = this.contentForm.controls.gender.value;
      this.EmployeeList[this.updateIndex].department=this.contentForm.controls.department.value;
      this.EmployeeList[this.updateIndex].email=this.contentForm.controls.email.value;
      this.EmployeeList[this.updateIndex].address=this.contentForm.controls.address.value;
      this.update = false;
      this.updateIndex = -1;
      this.contentForm.reset();
      return;
      }
      this.EmployeeList.push(this.contentForm.value);
      this.table.renderRows();
      this.contentForm.reset();
    }
  } 

  reset = () =>{
    this.isSubmit = false;
    this.contentForm.reset();
  } 

  updateEmployee = (employee:any , index:number) =>{
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent , {
      data : {
        'message' : 'Do you really want to update this Informations?' + ' ' + this.serverName
      }
    });
    this.confirmationDialog.afterClosed().subscribe(result=>{
      if(result){
        this.update = true;
        this.updateIndex = index;
        this.contentForm.setValue(employee);
      }
    })
  }

  get fv(): { [key: string]: AbstractControl } {
    return this.contentForm.controls;
  }

  deleteEmployee = (index:number) =>{
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent,
      {
        data : {
          'message' :'Do yout really want to delete this information?'
        }
      });
      this.confirmationDialog.afterClosed().subscribe(result=>{
        if(result)
        {
          this.update =true;
          this.updateIndex = index;
          this.contentForm.setValue(this.deleteEmployee);
        }
      })
    this.EmployeeList.splice(index, 0);
    this.table.renderRows();
  }

  onClick = () =>{
    console.log('Click', this.text)
  } 

  onChangeEvent = (ev:any) =>{
    this.allowText = (<HTMLInputElement>ev.target.value)

  }
 



}

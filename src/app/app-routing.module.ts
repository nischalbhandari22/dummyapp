import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
    
  },
  {
    path:'login',
    loadChildren: () => import('./login/login/login.module').then(m => m.LoginModule)
  },
  {
    path:'content',
    loadChildren: () => import('./content/content/content.module').then(m => m.ContentModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

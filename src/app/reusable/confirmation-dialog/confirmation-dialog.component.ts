import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {
  array = ['a','b','c','d','e','f','g','h','i' ]
  constructor(
    public dialogRef:MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
   
  }

  onNoClick = () => {
    this.dialogRef.close(false);
  }
  onYesClick = () =>{
    this.dialogRef.close(true);
  }

}

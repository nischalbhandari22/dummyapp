import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   
  hide=true;
  loginForm : FormGroup;
  emailControl = new FormControl();
  passwordControl = new FormControl();

  
  constructor(private router:Router,formBuilder:FormBuilder) {
    this.loginForm =formBuilder.group({
      email:this.emailControl,
      password :this.passwordControl
    })
   
   }

  ngOnInit(): void { 
  
  }
  nagivateTo=()=>
  {
    this.router.navigate(['/content']);

  }
  onSubmit(){
    console.log(this.loginForm.value)
    if(this.loginForm.valid)
    {
      this.router.navigate(['/content']);

    }
  }

}
